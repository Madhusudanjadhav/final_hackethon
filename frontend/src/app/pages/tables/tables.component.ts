// import { Component, OnInit } from "@angular/core";

// @Component({
//   selector: "app-tables",
//   templateUrl: "tables.component.html"
// })
// export class TablesComponent implements OnInit {
//   constructor() {}

//   ngOnInit() {}
// }

import { Component, OnInit } from '@angular/core';
import { Trading } from 'src/app/shared/trading';
import { RestApiService } from "../../shared/rest-api.service";
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-tables',
  templateUrl: 'tables.component.html'
})
export class TablesComponent implements OnInit {

  Tradings: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadTradings()
  }

  loadTradings() {
    return this.restApi.getTrading().subscribe((data: {}) => {
        this.Tradings = data;
    })
  }

  deleteTrading(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteTrading(id).subscribe(data => {
        this.loadTradings()
      })
    }
  }
  reloadCurrentPage() {
    window.location.reload();
   }

   editValidation(status:number){
     if(status!=0){
      window.confirm('Order completed: Cannot be deleted or edited now!!')
    
      return false
     }
     else{
       return true
     }

   }
   trNew=null
  updateTradings(id:number, tr:Trading, vol:number) {
    if(this.editValidation(tr.statusCode)){
    if(window.confirm('Are you sure, you want to update?')){
      this.restApi.updateTradings(id, tr,vol).subscribe(data => {
        console.log(tr)
        console.log(id)
        this.loadTradings()
        this.reloadCurrentPage()
      })
    }
    }
  }  

}



