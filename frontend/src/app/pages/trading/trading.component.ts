import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { RestApiService } from "src/app/shared/rest-api.service";
import { newTrading } from "src/app/shared/new-trading";

@Component({
  selector: "app-trading",
  templateUrl: "trading.component.html"
})
export class TradingComponent implements OnInit {
  [x: string]: any;

  @Input() inputData = {volume:0 }
  @Output() public eventName: EventEmitter<any> = new EventEmitter();


  LiveTable: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }
  

  ngOnInit(): void {
    this.loadTradings()
  }

  tempData={
  stockTicker: null,
  current:0
  }
  reloadCurrentPage() {
    window.location.reload();
   }

  
  slackMessage(tr){
    this.restApi.createSlackMessage(tr);
  }
  slackSellMessage(tr){
    this.restApi.createSellSlackMessage(tr);
  }
  addTrading( pr:number, ticker:string, action:string) {
    var tr={
      buyOrSell: action,
    price: pr,
    statusCode: 0,
    stockTicker: ticker,
    volume: this.inputData.volume
    }
    this.restApi.createTradings(tr).subscribe((data: {}) => {
      this.loadTradings()
      this.reloadCurrentPage()
      if(tr.buyOrSell=="buy"){
        this.slackMessage(tr)
      }
      else{
        this.slackSellMessage(tr)
      }
      
    })
  }
  loadTradings() {
    return this.restApi.getLiveData().subscribe((data: {}) => {
        this.LiveTable = data;
    })
  }
}
