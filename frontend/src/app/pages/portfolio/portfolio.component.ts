import { Component, OnInit } from "@angular/core";
import { RestApiService } from "src/app/shared/rest-api.service";

declare const google: any;



@Component({
  selector: "app-portfolio",
  templateUrl: "portfolio.component.html"
})
export class PortfolioComponent implements OnInit {
  PortfolioTradings: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit() {
    this.loadTradings()
  }
   temp=400;
   change=0;
  percentChange(price:number){
    this.change=((this.temp-price)/price)*100;
    console.log(this.change)
    return this.change;
  }
  loadTradings() {
    return this.restApi.getTrading().subscribe((data: {}) => {
        this.PortfolioTradings = data;
    })
  }
}
