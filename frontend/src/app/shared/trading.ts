// export class Trading {

// }

export class Trading {
    constructor(){
        this.id=0;
        this.stockTicker="";
        this.price=0;
        this.volume=0;
        this.buyOrSell = "";
		this.statusCode = 0;
    }
    id: number;
    stockTicker: string;
    price: number;
    volume: number;
    buyOrSell: string;
	statusCode: number;
}