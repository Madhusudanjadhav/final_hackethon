// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class RestApiService {

//   constructor() { }
// }

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Trading } from "./trading";
import {LiveTable} from "./live-table";
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { newTrading } from './new-trading';
import { SingleStock } from './single-stock';
//import { Observable } from 'rxjs/dist/types/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  apiURL = 'http://localhost:8080/api/Tradings/';
  liveURL= 'http://localhost:8080/api/LiveTable/';
  slackURL= 'http://localhost:8081/apps/';
  singleURL= 'http://localhost:8080/api/Portfolio/';
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  getLiveData():Observable<LiveTable>{
    return this.http.get<LiveTable>(this.liveURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  getLiveDataByName(name:string): Observable<LiveTable> {
    return this.http.get<LiveTable>(this.liveURL + name)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 
  // HttpClient API get() method 
  getTrading(): Observable<Trading> {
    return this.http.get<Trading>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API get() method 
  getTradings(id:any): Observable<Trading> {
    return this.http.get<Trading>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // HttpClient API post() method 
  createTradings(Tradings:newTrading): Observable<Trading> {
    return this.http.post<Trading>(this.apiURL + '', JSON.stringify(Tradings), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  } 

  // HttpClient API put() method
  updateTradings(id:number, Tradings:Trading, vol:number): Observable<Trading> {
    var temp={buyOrSell: Tradings.buyOrSell,
    id: Tradings.id,
    price: Tradings.price,
    statusCode: Tradings.statusCode,
    stockTicker: Tradings.stockTicker,
    volume: vol}
    console.log(temp)
    return this.http.put<Trading>(this.apiURL, JSON.stringify(temp), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method 
  deleteTrading(id:number){
    return this.http.delete<Trading>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
  createSlackMessage(tr : any){
    var message = 'Your buy request for stock ' + tr.stockTicker + " has been generated";
    var url = this.slackURL + message;
    console.log(url)
    fetch(url, {
      method: "POST"
    }).then(res => {
      console.log("Request complete! response:", res);
    });
  }
  createSellSlackMessage(tr : any){
    var message = 'Your sell request for stock ' + tr.stockTicker + " has been generated";
    var url = this.slackURL + message;
    console.log(url)
    fetch(url, {
      method: "POST"
    }).then(res => {
      console.log("Request complete! response:", res);
    });
  }
  getLiveDataofSingleStock():Observable<SingleStock>{
    return this.http.get<SingleStock>(this.singleURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }

 
  
}
