export class newTrading {
    constructor(){
        this.stockTicker="";
        this.price=0;
        this.volume=0;
        this.buyOrSell = "";
		this.statusCode = 0;
    }
    stockTicker: string;
    price: number;
    volume: number;
    buyOrSell: string;
	statusCode: number;
}
