// export class Trading {

// }

export class LiveTable {
    constructor(){
        this.stockTicker="";
        this.open=0;
        this.high=0;
        this.low = 0;
		this.current = 0;
    }
    stockTicker: string;
    open: number;
    high: number;
    low: number;
	current: number;
}