# -*- coding: utf-8 -*-
"""
Created on Fri Aug 27 11:34:29 2021

@author: Administrator
"""

import yfinance as yf

msft = yf.Ticker("MSFT")

# get stock info
#print(msft.info)

# get historical market data
hist = msft.history(period="6mo")
print(hist)

hist1 = hist[['Open','High','Low',"Close",'Volume']]
print(hist1)

hreset = hist1.reset_index()

hist2 = hreset[['Date','Open','High','Low',"Close",'Volume']]

#abc =[['r','g'],['r','g']]



Amazon = yf.Ticker("AMZN") 
print(Amazon.history(period="max"))


import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="c0nygre",
  database="trading"
)

mycursor = mydb.cursor()


# creating column list for insertion
cols = "`,`".join([str(i) for i in hist2.columns.tolist()])

# Insert DataFrame recrds one by one.
for i,row in hist2.iterrows():
    sql = "INSERT INTO `live_trading_table` (`" +cols + "`) VALUES (" + "%s,"*(len(row)-1) + "%s)"
    mycursor.execute(sql, tuple(row))

    # the connection is not autocommitted by default, so we must commit to save our changes
    mydb.commit()
