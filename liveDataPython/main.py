import yfinance as yf

import plotly.graph_objects as go
import pandas as pd
from pandas_datareader import data as pdr
import mplfinance as fplt



msft = yf.Ticker("MSFT")

# get stock info
#print(msft.info)

# get historical market data
hist = msft.history(period="1mo")
print(hist)

hist1 = hist[['Open','High','Low',"Close",'Volume']]
print(hist1)

hreset = hist1.reset_index()

df = hreset[['Date','Open','High','Low',"Close",'Volume']]
index_vals = df['Date'].astype('category').cat.codes

fig = go.Figure(data=go.Splom(
                dimensions=[dict(label='Open',
                                 values=df['Open']),
                            dict(label='High',
                                 values=df['High']),
                            dict(label='Low',
                                 values=df['Low']),
                            dict(label='Close',
                                 values=df['Close'])],
                showupperhalf=False, # remove plots on diagonal
                text=df['Date'],
                marker=dict(color=index_vals,
                            showscale=False, # colors encode categorical variables
                            line_color='white', line_width=0.5)
                ))


fig.update_layout(
    title='Data Set',
    width=1000,
    height=1000,
)

fig.show()

fig.write_html("C:/Users/Administrator/Desktop/final_hackethon/Metrix.html")


fplt.plot(hist,type='renko',renko_params=dict(brick_size=2.5),style='yahoo',figsize =(18,7),savefig="C:/Users/Administrator/Desktop/final_hackethon/renko.jpg")