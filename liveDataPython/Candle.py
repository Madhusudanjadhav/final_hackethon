# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 06:08:13 2021

@author: Administrator
"""


import plotly.graph_objects as go

import pandas as pd
from datetime import datetime

import yfinance as yf

msft = yf.Ticker("MSFT")
infy = yf.Ticker("INFY")

# get stock info
#print(msft.info)

# get historical market data
hist = msft.history(period="6mo")
print(hist)

histinfy = infy.history(period="5d")
print(histinfy)

hist1 = hist[['Open','High','Low',"Close",'Volume']]
print(hist1)

hreset = hist1.reset_index()
hreset2 = histinfy.reset_index()

hist2 = hreset[['Date','Open','High','Low',"Close",'Volume']]
hist3 = hreset2[['Date','Open','High','Low',"Close",'Volume']]


forks = hist3.sort_values(by='Volume',ascending=False)
vol = forks['Volume']
numdate = forks['Date']

#Amazon = yf.Ticker("AMZN") 
#print(Amazon.history(period="max"))

fig = go.Figure(data=[go.Candlestick(x=hist2['Date'],
                open=hist2['Open'],
                high=hist2['High'],
                low=hist2['Low'],
                close=hist2['Close'])])

fig.show()

fig.write_html("C:/Users/Administrator/Desktop/final_hackethon/candlesticks.html")

jj = ['08-24','08-27','08-23','08-26','08-25']

#jj =['2021-08-24,2021-08-18,2021-08-19,2021-08-17,2021-08-27,2021-08-23,2021-08-26,2021-08-25,2021-08-16,2021-08-20


import plotly.express as px
data = dict(
    num=forks["Volume"],
    stock_ticker=jj)
fig = px.funnel(data, x='num', y='stock_ticker')
fig.show()

fig.write_html("C:/Users/Administrator/Desktop/final_hackethon/funel.html")
