package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.LiveTable;
import com.example.demo.repository.LiveTableRepository;

@Service
public class LiveTableService {
	@Autowired
	private LiveTableRepository repository;
	
	public List<LiveTable> getAllLiveTable(){
		return repository.getAllLiveTable();
	}
	
	public LiveTable getLiveTableByname(String id) {
		return repository.getLiveTableByname(id);
	}

}
