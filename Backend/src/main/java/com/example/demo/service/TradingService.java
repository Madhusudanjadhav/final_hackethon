package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Trading;
import com.example.demo.repository.TradingRepository;

@Service
public class TradingService {
	@Autowired
	private TradingRepository repository;
	
	public List<Trading> getAllTradings(){		
		return repository.getAllTradings();
	}
	
	public Trading getTrading(int id) {
		if(id > 0) {
			return repository.getTradingById(id);
        }
        else {
            throw new IllegalArgumentException("The id value must be greater than zero.");
        }
	}

	public Trading saveTrading(Trading Trading) {
		if(Trading.getId() >0 && (Trading.getBuyOrSell().equals("buy") || Trading.getBuyOrSell().equals("sell")) 
				&& Trading.getPrice() >0 && Trading.getVolume()>0 ) {
		return repository.editTrading(Trading);
		}
		else {
			throw new IllegalArgumentException("Invalid Inputs");
		}
	}

	public Trading newTrading(Trading Trading) {
		if( (Trading.getBuyOrSell().equals("buy") || Trading.getBuyOrSell().equals("sell")) 
				&& Trading.getPrice() >0 && Trading.getVolume()>0 ) {
			return repository.addTrading(Trading);
		}
		else {
			throw new IllegalArgumentException("Invalid Inputs");
		}
	}

	public int deleteTrading(int id) {
		if(id > 0) {
			return repository.deleteTrading(id);
        }
        else {
            throw new IllegalArgumentException("The id value must be greater than zero.");
        }
		
	}
}
