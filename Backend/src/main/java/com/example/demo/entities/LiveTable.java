package com.example.demo.entities;

public class LiveTable {

	private String stockTicker;
	private double Open;
	private double High;
	private double Low;
	private double Current;
	
	public LiveTable() {
		
	}
	

	public LiveTable(String stockTicker, double open, double high, double low, double current) {
		super();
		this.stockTicker = stockTicker;
		Open = open;
		High = high;
		Low = low;
		Current = current;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getOpen() {
		return Open;
	}

	public void setOpen(double open) {
		Open = open;
	}

	public double getHigh() {
		return High;
	}

	public void setHigh(double high) {
		High = high;
	}

	public double getLow() {
		return Low;
	}

	public void setLow(double low) {
		Low = low;
	}

	public double getCurrent() {
		return Current;
	}

	public void setCurrent(double current) {
		Current = current;
	}
}
