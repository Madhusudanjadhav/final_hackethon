package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Trading;
import com.example.demo.service.TradingService;

@RestController
@CrossOrigin
@RequestMapping("api/Tradings")
public class TradingController {
	
	@Autowired
	TradingService service;
	
	@GetMapping(value = "/")
	public List<Trading> getAllTrading() {
		return service.getAllTradings();
	}
	
	@GetMapping(value = "/{id}")
	public Trading getTradingById(@PathVariable("id") int id) {
	  return service.getTrading(id);
	}

	@PostMapping(value = "/")
	public Trading addTrading(@RequestBody Trading Trading) {
		return service.newTrading(Trading);
	}

	@PutMapping(value = "/")
	public Trading editTrading(@RequestBody Trading Trading) {
		return service.saveTrading(Trading);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteTrading(@PathVariable int id) {
		return service.deleteTrading(id);
	}

}
