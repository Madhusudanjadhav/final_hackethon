package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Trading;

@Component
public interface TradingRepository {
	public List<Trading> getAllTradings();
	public Trading getTradingById(int id); 
	public Trading editTrading(Trading Trading);
	public int deleteTrading(int id);
	public Trading addTrading(Trading Trading);
	
}
