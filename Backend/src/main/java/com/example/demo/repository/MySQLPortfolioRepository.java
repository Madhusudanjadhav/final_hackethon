package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Portfolio;

@Repository
public class MySQLPortfolioRepository implements PortfolioRepository {
	
	@Autowired
	JdbcTemplate template;

	@Override
	public List<Portfolio> getAllPortfolio() {
		// TODO Auto-generated method stub
		String sql = "SELECT date, open, high, low, close, volume FROM live_trading_table";
		return template.query(sql, new PortfolioRowMapper());

	}

	

}

class PortfolioRowMapper implements RowMapper<Portfolio>{

	@Override
	public Portfolio mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return new Portfolio(
				rs.getString("date"),
				rs.getDouble("open"),
				rs.getDouble("high"),
				rs.getDouble("low"),
				rs.getDouble("close"),
				rs.getInt("Volume"));
	}
	
}

