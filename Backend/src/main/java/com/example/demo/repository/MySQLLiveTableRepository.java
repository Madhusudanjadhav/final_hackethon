package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.LiveTable;
@Repository
public class MySQLLiveTableRepository implements LiveTableRepository{
	
	@Autowired
	JdbcTemplate template;
	
	@Override
	public List<LiveTable> getAllLiveTable() {
		// TODO Auto-generated method stub
		String sql = "SELECT stockticker, Open, High, Low, Curr FROM data_sheet";
		return template.query(sql, new LiveTableRowMapper());

	}

	@Override
	public LiveTable getLiveTableByname(String id) {
		// TODO Auto-generated method stub
		String sql = "SELECT stockticker, Open, High, Low, Curr FROM data_sheet WHERE stockticker=?";
		return template.queryForObject(sql, new LiveTableRowMapper(), id);
	}
	
}

class LiveTableRowMapper implements RowMapper<LiveTable>{

	@Override
	public LiveTable mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return new LiveTable(rs.getString("stockticker"),
				rs.getDouble("Open"),
				rs.getDouble("High"),
				rs.getDouble("Low"),
				rs.getDouble("Curr")
				);
	}
	
}
