package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Portfolio;

@Component
public interface PortfolioRepository {
	public List<Portfolio> getAllPortfolio();
}
