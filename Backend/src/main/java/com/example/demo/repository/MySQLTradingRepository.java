package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Trading;

@Repository
public class MySQLTradingRepository implements TradingRepository{
	
	@Autowired
	JdbcTemplate template;
	
	@Override
	public List<Trading> getAllTradings() {
		// TODO Auto-generated method stub
		String sql = "SELECT buyOrSell, ID, price, statusCode, stockTicker, volume FROM trading";
		return template.query(sql, new TradingRowMapper());

	}

	@Override
	public Trading getTradingById(int id) {
		// TODO Auto-generated method stub
		String sql = "SELECT buyOrSell, ID, price, statusCode, stockTicker, volume FROM trading WHERE ID=?";
		return template.queryForObject(sql, new TradingRowMapper(), id);
	}

	@Override
	public Trading editTrading(Trading Trading) {
		// TODO Auto-generated method stub
		String sql = "UPDATE trading SET buyOrSell = ?, stockTicker = ?, price = ?, statusCode = ?, volume = ? " +
				"WHERE ID = ?";
		template.update(sql,Trading.getBuyOrSell(), Trading.getStockTicker(), Trading.getPrice(), Trading.getStatusCode(),Trading.getVolume(),Trading.getId());
		return Trading;
	}

	@Override
	public int deleteTrading(int id) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM trading WHERE ID = ?";
		template.update(sql,id);
		return id;
	}

	@Override
	public Trading addTrading(Trading Trading) {
		// TODO Auto-generated method stub
		String sql = "INSERT INTO trading(buyOrSell, price, statusCode, stockTicker, volume) " +
				"VALUES(?,?,?,?,?)";
		template.update(sql,Trading.getBuyOrSell(), Trading.getPrice(), Trading.getStatusCode(), Trading.getStockTicker(),Trading.getVolume());
		return Trading;
	}


	
}

class TradingRowMapper implements RowMapper<Trading>{

	@Override
	public Trading mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		return new Trading(rs.getString("BuyOrSell"),
				rs.getInt("ID"),
				rs.getDouble("Price"),
				rs.getString("StockTicker"), 
				rs.getInt("statusCode"),  
				rs.getInt("volume"));
	}
	
}
