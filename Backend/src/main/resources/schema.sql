use trading;
CREATE TABLE trading (
    id int NOT NULL AUTO_INCREMENT,
    stockTicker varchar(255) NOT NULL,
    price double,
    volume int,
    buyOrSell varchar(255) NOT NULL,
    statusCode int,
    PRIMARY KEY (id)
);
CREATE TABLE portfolio (
    id int NOT NULL AUTO_INCREMENT,
    stockTicker varchar(255) NOT NULL,
    price double,
    volume int,
    time_stamp timestamp, 
    PRIMARY KEY (id)
);