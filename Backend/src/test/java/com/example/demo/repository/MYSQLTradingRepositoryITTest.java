package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entities.Trading;

@SpringBootTest
public class MYSQLTradingRepositoryITTest {
	@Autowired
	TradingRepository repo;
	@Test
    public void getAllTradingsReturnsListOfTrades() {


        List<Trading> result = repo.getAllTradings();

       assertThat(result.size()>0);
       
    }

}
