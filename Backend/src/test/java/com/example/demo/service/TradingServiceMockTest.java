package com.example.demo.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.doReturn;

import org.mockito.Mock;

import com.example.demo.entities.Trading;
import com.example.demo.repository.TradingRepository;

@SpringBootTest

public class TradingServiceMockTest {
	@MockBean
    private TradingRepository repo;
	@Autowired
	TradingService service;

    @BeforeEach
    void setUp() {
        initMocks(this);

    }
    @Test
    public void getAllTradingsReturnsListOfTrades() {
    	  List<Trading> result = new ArrayList<Trading>();
    	  result.add(new Trading("buy", 200, 200, "sample",  0, 11));
    	
    //	  doReturn(1).when(repo).deleteTrading(1);
      doReturn(result).when(repo).getAllTradings();
      
      List<Trading> result2 = service.getAllTradings();//actual

       assertThat(result2.size()>0);
       
       
    }
}
