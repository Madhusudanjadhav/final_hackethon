package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entities.Trading;

@SpringBootTest
public class TradingServiceITTest {
	@Autowired
	TradingService service;
	@Test
    public void getAllTradingsReturnsListOfTrades() {

        List<Trading> result = service.getAllTradings();
       assertThat(result.size()>0);
      
    }
	@Test
    void getById_exception() {
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    service.getTrading(-1);
                });
        assertEquals("The id value must be greater than zero.", exception.getMessage());
    }
	@Test
    void deleteById_exception() {
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    service.deleteTrading(-1);
                });
        assertEquals("The id value must be greater than zero.", exception.getMessage());
    }
	@Test
	void edit_exception() {
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                	Trading tr= new Trading("BUYY", 98, -10, "xyz",0, 10);
                    service.saveTrading(tr);
                });
        assertEquals("Invalid Inputs", exception.getMessage());
    }
	@Test
	void add_exception() {
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                	Trading tr= new Trading("BUY", 98, 10, "xyz",0, -10);
                    service.newTrading(tr);
                });
        assertEquals("Invalid Inputs", exception.getMessage());
    }

}
