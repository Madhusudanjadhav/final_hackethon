package com.example.demo.entities;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class TradingTest {
	Trading trading=new Trading("buy", 999, 200, "sample",  0, 11);
	@Test
    public void constructorInitializesTradingObjectWithExpectedValues() {
		assertEquals(trading.getBuyOrSell(),"buy");
		assertEquals(trading.getStockTicker(),"sample");
		assertEquals(trading.getId(),999);
		assertEquals(trading.getPrice(),200);
		assertEquals(trading.getStatusCode(),0);
		assertEquals(trading.getVolume(),11);
		//check frank's API for trade from bitbucket

	}

}
